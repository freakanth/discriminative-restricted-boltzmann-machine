# Discriminative Restricted Boltzmann Machine
This repository contains code for training and evaluating the Discriminative 
Restricted Boltzmann Machines (DRBM). The implementation is in Python and
mainly based on Theano.

## Introduction
The DRBM was first introduced in [1]. This implementation of the DRBM includes 
two extensions proposed in [2] to the original model. These are the *Bipolar 
DRBM* and the *Binomial DRBM* whose cost functions have been derived assuming 
the {-1, +1}-Bernoulli distribution and Binomial distribution in the hidden 
layers of the model respectively, as a consequence of the theory presented in 
[2].

## Required Python libraries
The code requires the following Python modules to run:

* collections
* config
* cPickle
* getopt
* gzip
* itertools
* numpy
* sklearn
* theano

Any of these can be installed using the Python *pip* program. Other standard
Python modules are also required, but have not been listed here as they are 
contained in any Python distribution.

This code has only been tested only with Python 2.7.3 and the latest versions
of the above modules as on 08-04-2016.

Moreover, the scripts for reproducing the results have been written for BASH
and it is hence assumed that this shell is available on your machine.

## Usage
The code can be used for: 

1. Reproducing the results reported in [2].
2. Classifying data with the implemented models.
3. Building upon the work presented in [2].

### 1. Reproducing the results reported in [2]
Follow the below instructions in order to reproduce the results reported in
[2]:

1. Download the file ***cherla-et-al-ecml-2016-data.tar.bz2*** from
   *http://dx.doi.org/10.7910/DVN/EI5VRQ* (Harvard Dataverse repository link).

2. Extract the contents of the archive into the base folder of this repository
   such that following three folders are accessible:
  - *discriminative-restricted-boltzmann-machine/data/mnist*
  - *discriminative-restricted-boltzmann-machine/data/usps*
  - *discriminative-restricted-boltzmann-machine/data/20newgroups*

3. Select any one of the 15 folders corresponding to the models evaluated in
   [2]. In order to reproduce the result reported for the chosen model (say 
   *experiments/mnist/basic-drbm*, execute the *run_experiment.sh* script with
   the path to the experiment folder, as follows:

```
./run_experiment.sh experiments/mnist/basic-drbm
```

4. Once this script has finished executing, the trained model and its result
   are saved in the files
   *experiments/mnist/basic-drbm/results/mnist-results.tar.bz2* and 
   *experiments/mnist/basic-drbm/results/mnist-results.txt* respectively.

### 2. Classifying data with the implemented models
In order to use the model in other classification scenarios, follow the below
instructions:

1. In order to train a model on a given dataset, run the following command

```
python train_model.py -m path/to/model-config.cfg -d path/to/dataset-list.txt -o path/to/optimiser-config.cfg
```

2. And the trained model can be evaluated with the following command:
  
```
python test_model.py -d path/to/dataset-list.txt -e path/to/evaluator-config.cfg
```

For examples of the different configuration files above (*model-config.cfg*, 
*optimiser-config.cfg* and *evaluator-config.cfg*) and the dataset list file
(*dataset-list.txt*), refer to any of the experiment folders in the *experiments* 
folder mentioned above.

### 3. Building upon the work presented in [2]
If you use this code in other work, please cite [2], and acknowledge our
effort. This will be much appreciated.


## References
[1] Larochelle, H., & Bengio, Y. (2008, July). Classification using
discriminative restricted Boltzmann machines. In Proceedings of the 25th
international conference on Machine learning (pp. 536-543). ACM.

[2] Cherla, S., Tran, S., Weyde, T., & d'Avila Garcez, A. (2016, April).
Generalising the Discriminative Restricted Boltzmann Machine. arXiv preprint
arXiv:1604.01806.
