"""Various utility functions.

dict_product:
  Convert dict(list) to list(dict) by taking the Cartesian product.

index_to_onehot:
  Convert a sequence of integer indices into a one-hot vector sequence.

onehot_to_index:
  Convert a sequence of one-hot vectors into a sequence of integer indices.
"""


from collections import OrderedDict
from itertools import product
from itertools import izip
import numpy as np
from sklearn.utils import gen_even_slices
import sys

RNG = np.random.RandomState(860331)


def dict_combine(dict1, dict2):
    """Combine the keys of two dictionaries into a single dictionary. It is
    important here that the order of keys is the same each time the same types
    of dictionaries are combined, as they are traversed sequentially to
    generate file names, etc.

    Input
    -----
    dict1 : dict
      First dictionary
    dict2 : dict
      Second dictionary

    Output
    ------
    dictc : dict
      Combined dictionary
    """
    dictc = OrderedDict()
    for k, e in dict1.items() + dict2.items():
        dictc.setdefault(k, e)

    return dictc


def dict_product(dicts):
    """Convert dict(list) to list(dict) by taking the Cartesian product.

    Input
    -----
    dicts: dict
      A dictionary where each key is associated with a list of values.

    Output
    ------
    -----: list
      A list where each element is obtained after the Cartesian product over
      the values associated with different keys.
    """
    return (OrderedDict(izip(dicts, e)) 
            for e in product(*dicts.itervalues()))


def make_batches(data_x, data_y, batch_size):
    """Split dataset into batches.
    """
    n_data = data_x.shape[0]
    # Compute number of minibatches for training, validation and testing.
    n_batches = np.int(np.ceil(np.float(n_data) / batch_size))
    batch_slices = list(gen_even_slices(n_data, n_batches))

    batches_x = [data_x[batch_slice] for batch_slice in batch_slices]
    batches_y = [data_y[batch_slice] for batch_slice in batch_slices]

    return batches_x, batches_y
