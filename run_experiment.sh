#!/bin/bash

# Root directory of the experiment and subfolders
EXPT_ROOT=$1
DATA_ROOT="data"
echo "Running experiment set up in the folder $1"

# Suffixes for various types of files
MOD_SUFFIX="-mod.cfg"
OPT_SUFFIX="-opt.cfg"
EVA_SUFFIX="-eva.cfg"
RESULTS_SUFFIX="-results.txt"

# Check if datasets folder exists
DATASETS_DIR="$EXPT_ROOT/datasets"
if [ ! -d $DATASETS_DIR ]
then
    echo "Folder with list of datasets - $DATASETS_DIR, doesn't exist."
    exit $?
fi
echo "Folder with list of datasets - $DATASETS_DIR, exists."

# Check if results folder exists, if not, create it
RESULTS_DIR="$EXPT_ROOT/results"
if [ ! -d $RESULTS_DIR ]
then
    echo "Folder for results - $RESULTS_DIR, doesn't exist. Created it."
    mkdir $RESULTS_DIR
else
    echo "Folder for results - $RESULTS_DIR, exists."
fi

# Check if model configuration folder exists
MOD_CFG_DIR="$EXPT_ROOT/models"
if [ ! -d $MOD_CFG_DIR ]
then
    echo "Folder with model configurations - $MOD_CFG_DIR, doesn't exist."
    exit $?
fi

# Check if optimizer configuration folder exists
OPT_CFG_DIR="$EXPT_ROOT/optimizers"
if [ ! -d $OPT_CFG_DIR ]
then
    echo "Folder with optimizer configurations - $OPT_CFG_DIR, doesn't exist."
    exit $?
fi

# Check if evaluation configuration folder exists
EVA_CFG_DIR="$EXPT_ROOT/evaluators"
if [ ! -d $EVA_CFG_DIR ]
then
    echo "Folder with evaluator configurations - $EVA_CFG_DIR, doesn't exist."
    exit $?
fi

# Check if there exist model configuration files for each dataset or
# alternatively one file for all datasets.
SAME_MOD_CFG=0
if [ -f "$MOD_CFG_DIR/all$MOD_SUFFIX" ]
then
    echo "Using the same models specified in $MOD_CFG_DIR/all$MOD_SUFFIX with 
          all datasets."
    SAME_MOD_CFG=1
else
    for dfile in `ls -L $DATASET_DIR | egrep '*-dataset.txt'`
    do
        dname=`sed -s s/-dataset.txt// <<< $dfile`
        mfile="$MOD_CFG_DIR/$dname$MOD_SUFFIX"

        if [ ! -f $mfile ]
        then
            echo "Model configuration file for $dname dataset doesn't exist. 
                  Exiting..." 
            exit $?
        fi
    done
fi

# Check if there exist optimization configuration files for each dataset or
# alternatively one file for all datasets.
SAME_OPT_CFG=0
if [ -f "$OPT_CFG_DIR/all$OPT_SUFFIX" ]
then
    echo "Using the same optimizers specified in $OPT_CFG_DIR/all$OPT_SUFFIX 
          with models for all datasets."
    SAME_OPT_CFG=1
else
    for dfile in `ls -L $DATASET_DIR | egrep '*-dataset.txt'`
    do
        dname=`sed -s s/-dataset.txt// <<< $dfile`
        ofile="$OPT_CFG_DIR/$dname$OPT_SUFFIX"

        if [ ! -f $mfile ]
        then
            echo "Optimizer configuration file for $dname dataset doesn't exist 
                  Exiting..."
            exit $?
        fi
    done
fi

# Check if there exist evaluation configuration files for each dataset or
# alternatively one file for all datasets.
SAME_EVA_CFG=0
if [ -f "$EVA_CFG_DIR/all$EVA_SUFFIX" ]
then
    echo "Using the same evaluators specified in $EVA_CFG_DIR/all$EVA_SUFFIX 
          with models for all datasets."
    SAME_EVA_CFG=1
else
    for dfile in `ls -L $DATASET_DIR | egrep '*-dataset.txt'`
    do
        dname=`sed -s s/-dataset.txt// <<< $dfile`
        efile="$EVA_CFG_DIR/$dname$EVA_SUFFIX"

        if [ ! -f $efile ]
        then
            echo "Evaluation configuration file for $dname dataset doesn't
                  exist. Exiting..."
            exit $?
        fi
    done
fi

# for dtst in ${DATASETS[@]} # Loop condition when manually listing datasets

# AND FINALLY train and test models over all the datasets
for dfile in `ls -L $DATASETS_DIR | egrep '*-dataset.txt'`
do
    dname=`sed -s s/-dataset.txt// <<< $dfile`
    echo "Dataset: $dname"
    dat_file="$DATASETS_DIR/$dfile" 
    res_file="$RESULTS_DIR/$dname$RESULTS_SUFFIX"
    
    if [ $SAME_MOD_CFG -eq 1 ]
    then
        mod_file="$MOD_CFG_DIR/all$MOD_SUFFIX"
    else
        mod_file="$MOD_CFG_DIR/$dname$MOD_SUFFIX"
    fi

    if [ $SAME_OPT_CFG -eq 1 ]
    then
        opt_file="$OPT_CFG_DIR/all$OPT_SUFFIX"
    else
        opt_file="$OPT_CFG_DIR/$dname$OPT_SUFFIX"
    fi

    if [ $SAME_EVA_CFG -eq 1 ]
    then
        eva_file="$EVA_CFG_DIR/all$EVA_SUFFIX"
    else
        eva_file="$EVA_CFG_DIR/$dname$EVA_SUFFIX"
    fi

    # Train model
    echo "./train_models.py -d $dat_file -m $mod_file -o $opt_file"
    python train_models.py -d $dat_file -m $mod_file -o $opt_file

    # Test model
    echo "./test_models.py -d $dat_file -e $eva_file"
    python test_models.py -d $dat_file -e $eva_file

    # Save results 
    echo ""
    echo "python save_results.py -d $dat_file -o $res_file"
    python save_results.py -d $dat_file -o $res_file

    # Archive evaluated models
    echo ""
    echo "tar -cvjf $DATA_ROOT'/'$dname'.tar.bz2' $DATA_ROOT'/'$dname"
    tar -cvjf $DATA_ROOT"/"$dname"-results.tar.bz2" $DATA_ROOT"/"$dname
    mv $DATA_ROOT"/"$dname"-results.tar.bz2" $RESULTS_DIR
    find $DATA_ROOT"/"$dname"/models/" -name "*.pkl.gz" | xargs rm

    # Compute mean and standard deviations of 
    tmp_res_file="$RESULTS_DIR/$dname$RESULTS_SUFFIX.tmp"

    awk '/mean_valid_crosent/ {print $0}' \
        $res_file > $tmp_res_file

    awk 'BEGIN {sum=0; sq_sum=0; n=0} \
         // {sum+=$23; sq_sum+=$23^2; n++} \
         END {mean= sum/n; std=sqrt((sq_sum - (sum^2)/n)/n); 
         printf("\tMean (test): %.3f; Std. (test): %.5f\n", mean*100, std)}' \
         $tmp_res_file > $res_file

    awk 'BEGIN {sum=0; sq_sum=0; n=0} \
         // {sum+=$3; sq_sum+=$3^2; n++} \
         END {mean= sum/n; std=sqrt((sq_sum - (sum^2)/n)/n); 
         printf("\tMean (valid): %.3f; Std. (valid): %.5f\n", mean*100, std)}' \
         $tmp_res_file >> $res_file

    rm $tmp_res_file
    cat $res_file
done

# XXX: Reference for BASH arrays.
#
#declare -a DATASETS=('nova-scotia' 'bach' 'elsass' 'jugoslav' 'schweiz' \
#    'oesterrh' 'kinder' 'shanxi')
#
#for dtst in ${DATASETS[@]} # WTF syntax?
#do
#    dataset_file="$ROOT/datasets/$dtst$DATASET_FILE_SUFFIX"
#    config_file="$ROOT/models/$CONFIG_FILE_PREFIX$dtst$CONFIG_FILE_SUFFIX"
#    python train_models.py -d $dataset_file -o $ROOT/pretrain.cfg -m $config_file
#    python test_models.py -d $dataset_file -e $ROOT/evaluate-semi-online.cfg
#done

